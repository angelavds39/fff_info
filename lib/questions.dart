import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'i18n.dart';
import 'responsiveDrawerScaffold.dart';

class QuestionsPage extends StatefulWidget {
  QuestionsPage({
    Key key,
  }) : super(key: key);

  @override
  _QuestionsPageState createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage> {
  @override
  Widget build(BuildContext context) {
    var faqData = [
      {
        'question': Internationalisation.of(context).translation("time"),
        'answer': Internationalisation.of(context).translation("time_answer")
      },
      {
        'question': Internationalisation.of(context).translation("what"),
        'answer': Internationalisation.of(context).translation("what_answer")
      },
      {
        'question': Internationalisation.of(context).translation("why"),
        'answer': Internationalisation.of(context).translation("why_answer"),
      },
      {
        'question':
            Internationalisation.of(context).translation("what_difference"),
        'answer': Internationalisation.of(context)
            .translation("what_difference_answer"),
      },
      {
        'question': Internationalisation.of(context).translation("who"),
        'answer': Internationalisation.of(context).translation("who_answer")
      },
      {
        'question': Internationalisation.of(context).translation("whatasking"),
        'answer':
            Internationalisation.of(context).translation("whatasking_answer")
      },
      {
        'question': Internationalisation.of(context).translation("whatif"),
        'answer': Internationalisation.of(context).translation("whatif_answer")
      },
      {
        'question': Internationalisation.of(context).translation("whosupports"),
        'answer':
            Internationalisation.of(context).translation("whosupports_answer")
      },
      {
        'question': Internationalisation.of(context).translation("adults"),
        'answer': Internationalisation.of(context).translation("adults_answer")
      },
    ];

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return ResponsiveDrawerScaffold(
      title: 'FAQ',
      body: Column(
        children: <Widget>[
          Text(
            Internationalisation.of(context).translation("FAQ"),
            style: Theme.of(context).textTheme.display1,
          ),
          Expanded(
              child: ListView.builder(
            itemCount: faqData.length,
            itemBuilder: (BuildContext context, int index) {
              return ExpansionTile(
                leading: Icon(FontAwesomeIcons.question),
                title: Text(
                  faqData[index]['question'],
                  style: Theme.of(context).textTheme.headline,
                ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(faqData[index]['answer']),
                  )
                ],
              );
            },
          )),
          Text(
            "Source: https://globalclimatestrike.net/#faq",
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );
  }
}
