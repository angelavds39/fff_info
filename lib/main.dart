import 'package:fff_info/strikelist.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride;
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'i18n.dart';
import 'theme.dart';

void main() {
  // See https://github.com/flutter/flutter/wiki/Desktop-shells#target-platform-override
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(ClimateTheme(
    child: new ClimateApp(),
    initialThemeKey: ClimateThemeKeys.GERMANY,
  ));
}

class ClimateApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        const DemoLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en'),
        // English
        const Locale('de'),
        // German
      ],
      title: '#FridaysForFuture',
      theme: ClimateTheme.of(context),
      home: StrikeListPage(),
    );
  }
}

class DemoLocalizationsDelegate
    extends LocalizationsDelegate<Internationalisation> {
  const DemoLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'de'].contains(locale.languageCode);

  @override
  Future<Internationalisation> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<Internationalisation>(
        Internationalisation(locale));
  }

  @override
  bool shouldReload(DemoLocalizationsDelegate old) => false;
}
