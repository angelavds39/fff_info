import 'package:fff_info/globals.dart';
import 'package:flutter/material.dart';

class Internationalisation {
  Internationalisation(this.locale);

  final Locale locale;

  static Internationalisation of(BuildContext context) {
    return Localizations.of<Internationalisation>(
        context, Internationalisation);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'numberoftrees': 'You have planted that many trees:',
      'join': 'Join the global climate strike from september 20th to 27th!',
      'time': 'When are the Global Climate Strikes?',
      'time_answer':
          'Young people have woken up much of the world with their powerful Fridays for Future school strikes for the climate. As we deal with devastating climate breakdown and hurtle towards dangerous tipping points, young people are calling on millions of us across the planet to disrupt business as usual by joining the global climate strikes on September 20, just ahead of a UN emergency climate summit, and again on September 27. Together, we will sound the alarm and show our politicians that business as usual is no longer an option. The climate crisis won’t wait, so neither will we.',
      'what': 'What is planned?',
      'what_answer':
          'Millions of school climate strikers have been leaving their classrooms every Friday. Now they hope that everyone else will join them in action. Going on climate strike means people everywhere walking out of their homes, their offices, their farms, their factories. Everyone is needed to disrupt business as usual: from sports stars, actors and teachers to food industry workers, psychologists, delivery drivers and everything in between. We can all take part, whatever our circumstances, by refusing to accept the status quo. Already people in 150 countries are organising for the global climate strikes this September. Some will spend the day in protest against new pipelines and mines, or the banks that fund them; some will highlight the oil companies fuelling this crisis and the politicians that enable them. Others will spend the day in action raising awareness in their communities and pushing for solutions to the climate crisis that have justice and equity at their heart. On a grossly unequal planet, not everyone will be able to take a day off or take part in the same way. But we can all take a stand and make our voices heard. Find out how.',
      'why': 'Why go on #ClimateStrike?',
      'why_answer':
          'Our house is on fire. The climate crisis is an emergency but we’re not acting like it. People everywhere are at risk if we let oil, coal and gas companies continue to pour more fuel on the fire. Our hotter planet is already hurting millions of people. If we don’t act now to transition fairly and swiftly away from fossil fuels to 100% renewable energy access for all, the injustice of the climate crisis will only get worse. We need to act right now to stop burning fossil fuels and ensure a rapid energy revolution with equity, reparations and climate justice at its heart. But it’s going to take all of us working together to succeed. Join the Climate Strikes starting on September 20 – three days out from the UN’s climate emergency summit and continuing on September 27. Millions of us will walk out from home, work, school or university to declare a climate emergency and show our politicians what action in line with climate science and justice means. The climate crisis won’t wait, so neither will we.',
      'what_difference': 'What difference will it make?',
      'what_difference_answer':
          'These Climate Strikes won’t solve the climate crisis alone. What this moment can do is demonstrate that people are no longer willing to continue with business as usual. The urgency of the climate crisis requires a new approach and a just response centred on human rights, equity, and justice. Greta Thunberg’s lone protest caught the world’s attention last year, and has spread to millions of school children who are sounding the alarm. Now it’s everyone’s turn to stand with young people and show world leaders the people power demanding climate justice. September’s climate strikes will kickstart a huge wave of action and renewed ambition all over the world.',
      'who': 'Who is going on climate strike?',
      'who_answer':
          'Everyone! School strikers are aiming to make September 20 their biggest mobilisation yet, whilst unions and adults everywhere are preparing their own climate strikes on September 20 and 27 in over 150 countries already. We already know that environmental, public health, social justice, and development groups will join in. But our greatest hope is simply to show that those working on this crisis have the backing of millions of human beings who have a growing dread about the climate emergency but who have so far stayed mostly on the sidelines. It will take all of our efforts to get millions of us in the streets worldwide. So join us. Our window for effective climate action is closing fast.',
      'whatasking': 'What are we asking for?',
      'whatasking_answer':
          'The climate crisis is an emergency – we want everyone to start acting like it. We demand climate justice for everyone. Our hotter planet is already hurting millions of people. If we don’t act now to transition fairly and swiftly away from fossil fuels to 100% renewable energy for all, the injustice of the climate crisis will only get worse. We need to act right now to stop burning fossil fuels and ensure a rapid energy revolution with equity, reparations and climate justice at its heart.',
      'whatif': 'What if I cant go on climate strike?',
      'whatif_answer':
          'Workers’ rights and labour laws vary hugely around the world, and not everyone can work, strike or be part of a union. On a grossly unequal planet, some of us can’t do without a single day’s pay, and some of us work for bosses who would fire us if we dared try. And some jobs simply can’t stop: emergency room doctors should keep on saving lives. But many of us can put off for 24 hours our usual work, confident it will be there when we return. The strike called by youth on September 20 will kick start a week of climate action with many different plans underway in different parts of the world allowing adults to join together, step up and take action for our climate. Here is a range of ways you could join the Climate Strikes if you’re supposed to be working on September 20 or 27',
      'whosupports': 'Who is supporting climate strikes?',
      'whosupports_answer':
          'Climate strikes were started by school students organising through the #FridaysForFuture platform. This Global Climate Strike mobilisation is a joint response by a broad coalition of groups, NGOs, unions and social movements across the world to young people’s call to action for September. These are the principles guiding our collective efforts towards September.',
      'adults': 'Can adults join in?',
      'adults_answer':
          'Absolutely! School strikers have invited everyone to walk out in solidarity with them on Friday September 20 and 27. This is a global moment to show politicians everywhere that our movement is growing from strength to strength and we won’t stop until we get climate justice for everyone. This is only the beginning.',
      'FAQ': 'Frequently Asked Questions',
      'settings': 'Settings',
      'language': 'Language',
      'english': 'English',
      'german': 'German',
      'germany': 'Germany',
      'global': 'Global',
      'list': 'Strike List',
      'sfoc': 'Strike for our climate!',
      'home': 'Home',
      'allow_cal_access_msg':
          'Please allow us to add strikes and other events to your calender',
      'choose_cal': 'Choose calendar'
    },
    'de': {
      'numberoftrees': 'Anzahl der gepflantzten Bäume:',
      'join': 'Nehmen Sie am globalen Klimastreik 20. bis zum 27. teil',
      'time': 'Wann finden globale Klimastreiks statt?',
      'time_answer':
          'Junge Menschen haben mit ihren wirkungsvollen Streiks für das Klima bei Fridays for Future in der Schule einen Großteil der Welt aufgeweckt. Angesichts des verheerenden Klimawandels und der Gefahr, zu gefährlichen Wendepunkten zu gelangen, rufen junge Menschen Millionen von Menschen auf der ganzen Welt zum zivilen Ungehorsam auf, indem sie sich am 20. September an den globalen Klimaschutzstreiks unmittelbar vor der UN-Klimakonferenz beteiligen am 27. September erneut. Gemeinsam werden wir unseren Politikern zeigen, dass es nicht länger möglich ist, wie bisher zu handeln. Die Klimakrise wird nicht warten; wir auch nicht!',
      'what': 'Was ist geplant?',
      'what_answer':
          'Jeden Freitag streiken Millionen von Menschen und verlassen ihre Schulen. Sie hoffen, dass alle anderen auch mitmachen. Beim Klimastreik mitzumachen bedeutet, dass Menschen überall in ihren Büros, Farmen und Fabriken streiken und demonstrieren. Jeder muss die Arbeit stören: von Sportstars, Schauspielern und Lehrern bis hin zu Arbeitern der Lebensmittelindustrie, Psychologen, Auslieferungsfahrern und so weiter. Wir alle können mitmachen, indem wir uns weigern, den Status quo zu akzeptieren. Bereits im September organisierten sich Menschen in 150 Ländern für die globalen Klimastreiks. Einige werden den Tag aus Protest gegen neue Pipelines und Minen oder die Banken verbringen, die sie finanzieren; andere werden die Ölfirmen hervorheben, die diese Krise befeuern, und die Politiker, die sie befähigen. Andere werden den Tag in Aktion verbringen, um das Bewusstsein in ihren Gemeinden zu schärfen und nach Lösungen für die Klimakrise zu suchen, um Gerechtigkeit zu erlangen. In unserer ungleichen Welt wird jedoch nicht jeder in der Lage sein, sich einen Tag frei zu nehmen und daran teilzunehmen. Aber wir können alle unsere Meinung äußern und uns Gehör verschaffen. Finde heraus wie.',
      'why': 'Warum sollten wir am #KlimaStreik mitmachen?',
      'why_answer':
          'Unsere Welt brennt. Die Klimakrise ist in Notfall, aber wir verhalten uns nicht wie bei einem Notfall. Wir alle sind in Gefahr, wenn wir weiterhin Öl-, Kohle- und Gasunternehmen fossile Brennstoffe zu verbrennen. Bereits heute sind viele Mewnschen in Gefahr. Wenn wir nicht handeln, und den Übergang von fossilen Brennstoffen zu 100% durch erneuerbare Energien ersetzen, wird die Klimakrise noch fataler. Wir müssen sofort handeln, um die Verbrennung fossiler Kraftstoffe zu stoppen und eine rasche Energiewende zu gewähleisten, deren Kern aus Gerechtigkeit und Widergutmachung besteht. Trotzdem müssen wir alle zusammenarbeiten, um dabei erfolgreich zu sein. Schließe dich dem Klimastreik am 20. September an. Millionen Menschen werden von der Arbeit, Schule oder Uni fernbleiben, um auf die Klimakrise aufmerksam zu machen und unseren Politikern zu zeigen, dass sie handeln müssen. Die Klimakrise wartet nich und wir auch nicht!',
      'what_difference': "Was werden wir erreichen?",
      'what_difference_answer':
          'Klimastreiks allein werden die Klimakrise nicht lösen. Aber wir können demonstrieren, bis die Menschen bereit sind, nicht mehr wie gewohnt weiterzumachen. Die Dringlichkeit der Klimakrise erfordert einen neuen Ansatz und eine gerechte Reaktion, die sich auf Menschenrechte und Gerechtigkeit konzentriert. Der einsame Protest von Greta Thunberg erregte letztes Jahr Aufmerksamkeit in der ganzen Welt und hat sich auf Millionen von Schülern ausgeweitet, die mitmachen. Jetzt ist jeder an der Reihe, sich uns zu stellen und bei den Mächtigen der Welt Klimagerechtigkeit einfordern. Unsere Klimastreiks werden viel erreichen!',
      'who': 'Wer kann am Klimastreik teilnehmen?',
      'who_answer':
          'Jeder! Gewerkschaften und Erwachsene bereiten bereits ihre eigenen Klimastreiks vor. Wir wissen bereits, dass sich in Zukunft Umwelt-, Gesundheits-, Sozial- und Entwicklungsgruppen beteiligen werden. Unsere größte Hoffnung ist es jedoch zu zeigen, dass jeder vom Klimawandel betroffen sein wird. Wir müssen Millionen von Menschen mobilisieren und auf die Straße bringen. Also mach mit. Unser Handlungsmöglichkeiten für den Klimaschutz werden von Tag zu Tag geringer.',
      'whatasking': 'Was fordern wir?',
      'whatasking_answer':
          'Die Klimakrise ist ein ernstzunehmender Notfall - wir möchten, dass alle sich auch dementsprechend verhalten. Wir fordern Klimagerechtigkeit für alle. Bereits heute fallen der Klimakrise Tausende zum Opfer. Wenn wir jetzt nicht beginnen, die fossilen Brennstoffen durch 100% erneuerbarer Energie ersetzen, wird die Klimakrise noch schlimmer. Wir müssen sofort handeln, um die Verbrennung fossiler Brennstoffe zu stoppen und eine rasche Energiewende umzusetzen, bei der Gerechtigkeit und Wiedergutmachung im Mittelpunkt stehen.',
      'whatif': 'Was ist, wenn ich nicht am Klimastreik teilnehmen kann?',
      'whatif_answer':
          'Die Arbeitnehmerrechte sind weltweit sehr unterschiedlich, und nicht jeder kann streiken oder Teil einer Gewerkschaft sein. Auf einem extrem ungleichen Planeten können einige von uns nicht auf einen einzigen Tageslohn verzichten, und einige von uns arbeiten für Chefs, die uns entlassen würden, wenn wir streiken würden. Und manche Menschen können nicht streiken: Notärzte sollten weiterhin Leben retten. Aber viele von uns können ihre gewohnte Arbeit für 24 Stunden aufschieben und sind zuversichtlich, sei nach der Rückkehr zu erledigen. Der von der Jugend ausgerufene Streik wird auch Erwachsene dazu veranlassen, Maßnahmen für unser Klima zu ergreifen. ',
      'whosupports': 'Wer unterstützt die Klimastreiks?',
      'whosupports_answer':
          'Klimastreiks wurden von Schülern begonnen, die sich über die Plattform #FridaysForFuture organisierten. Diese Mobilisierung zum globalen Klimastreik ist eine gemeinsame Reaktion einer breiten Koalition von Gruppen, Nichtregierungsorganisationen, Gewerkschaften und sozialen Bewegungen auf den Aufruf junger Menschen zum Handeln. ',
      'adults': 'Können auch Erwachsene mitmachen?',
      'adults_answer':
          'Natürlich! Alle sind eingeladen freitags gemeinsam zu demonstrieren. Dies ist ein globaler Moment, um den Politikern überall zu zeigen, dass unsere Bewegung immer stärker wird und wir nicht aufhören werden, bis wir für alle Klimagerechtigkeit erreichen. Dies ist nur der Anfang.',
      'FAQ': 'Häufig gestellte Fragen',
      'settings': 'Einstellungen',
      'language': 'Sprache',
      'english': 'Englisch',
      'german': 'Deutsch',
      'germany': 'Deutschland',
      'global': 'International',
      'list': 'Streikübersicht',
      'sfoc': 'Streikt für unser Klima!',
      'home': 'Start',
      'allow_cal_access_msg':
          'Bitte erlaube uns Termine in deinem Kalender zu erstellen, damit wir die Klima-Streiks bei dir im Kalender anlegen können',
      'choose_cal': 'Kalender auswählen',
    },
  };

  String translation(String text) {
    return _localizedValues[LANG][text];
  }
}
