# <img src="assets/launcher.png" height="56" width="56" /> #FFF Info

![Started at Jugend hackt 2019](https://img.shields.io/badge/Started%20at-Jugend%20hackt%202019-%2300a6de?style=for-the-badge) ![On F-Droid](https://img.shields.io/f-droid/v/cf.fridays.fff_info.svg?style=for-the-badge)

Get all information about your climate strike!

## Getting Started

We try to offer an application, with which you get everything you need about #FridaysForFuture within one place.

Check out the web version on https://fridays.cf/

The app isn't ready yet. Feel free to contribute.

## Download

Download pre-build app for:
 * Android
    - [Direct Download](https://gitlab.com/JasMich.de/fridays-for-future_app/-/jobs/artifacts/master/download?job=build%3Aapk)
    - [On F-Droid](https://f-droid.org/en/packages/cf.fridays.fff_info/)
    - [In Google Play Store](https://play.google.com/store/apps/details?id=cf.fridays.fff_info)
 * [Linux Desktop](https://gitlab.com/JasMich.de/fridays-for-future_app/-/jobs/artifacts/master/download?job=build%3Alinux)
 * Web
    - [Web (JavaScript)](https://gitlab.com/JasMich.de/fridays-for-future_app/-/jobs/artifacts/master/download?job=build%3Aweb)
    - [Web site](https://fridays.cf/)


## Build

Install flutter first. See [flutter.dev](https://flutter.dev/docs/get-started/install) for more details.
```
flutter channel master # For stable builds, we recommend the beta channel.
                       # As Flutter's beta doesn't support Desktop-Shells yet,
                       # please use master for development...
flutter upgrade
flutter config --enable-web
flutter config --enable-linux-desktop
flutter config --enable-windows-desktop
flutter config --enable-macos-desktop
# Run Flutter doctor to check whether the installation was successful
flutter doctor
```

Connect any Android or iOS device (except if building for the web).

```
git clone https://gitlab.com/jugendhackt/fff_info.git
cd fff_info
flutter pub get
flutter run
# Or run as a web page
flutter run -d web
# Or for linux
flutter run -d linux
```

## Release

Build stable builds the following way:

```
flutter pub get

flutter build apk
flutter build appbundle

flutter build linux
flutter build windows # Is it working ?
flutter build macos # Is it working ?

flutter build ios

flutter build web
```

Releases can be found in `build/`.

## License

#FFF Info is licensed under the `EUPL-1.2`. See [LICENSE](LICENSE) for more information.
